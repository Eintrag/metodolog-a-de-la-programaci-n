import java.util.Scanner;

public class Main {
	public static void main(String [] args){
		
		Punto [] puntos = introdPuntos();
		if (puntos.length>1){
		//Solo tiene sentido calcular la distancia entre dos puntos o mas
			//Los ordenamos segun la coordenada X
			
			puntos = Quicksort.quicksortX(puntos,0,puntos.length-1);
			String texto="Puntos ordenados segun coordenada X: ";
			for(int i=0; i<puntos.length; i++){
				texto += "("+puntos[i].getX() + "," + puntos[i].getY() +") ";
			}
			imprimir(texto);
			//Resolvemos primero utilizando un algoritmo Divide y Venceras
			
			long tInicio=System.currentTimeMillis();  

			resolverDyV(puntos);
			
			long totalTiempo = System.currentTimeMillis() - tInicio; 
			System.out.println("Divide y Venceras tarda :" + totalTiempo + " miliseg"); 
			//Y ahora con un algoritmo de Fuerza Bruta
			tInicio=System.currentTimeMillis();
			
			resolverFB(puntos);
			
			totalTiempo = System.currentTimeMillis() - tInicio; 
			System.out.println("Fuerza bruta tarda :" + totalTiempo + " miliseg"); 

		}
		
		else imprimir("No tiene sentido calcular la distancia "
					+ "que existe entre menos de dos puntos");
	}
	
	public static double resolverFB(Punto[] puntos){
		//Algoritmo de fuerza bruta
		Punto[] parCercano = fuerzaBruta(puntos);
		double distancia = parCercano[0].distA(parCercano[1]);
		imprimir("Resultado Fuerza Bruta: La distancia mas corta es: " + distancia 
				+ " De los puntos " + parCercano[0].toString() 
				+" y "+ parCercano[1].toString());
		return distancia;
	}
	public static double resolverDyV(Punto[] puntos){
		//Algoritmo divide y venceras
		Punto [] parCercano = divideYVenceras(puntos, puntos.length);
		double distancia = parCercano[0].distA(parCercano[1]);
		imprimir("Resultado divideYVenceras: La distancia mas corta es: " + distancia 
				+ " De los puntos " + parCercano[0].toString() 
				+" y "+ parCercano[1].toString());
		return distancia;
	}
	public static void imprimir (String mensaje){
		System.out.println(mensaje);
	}
	public static double min (double a, double b){
		if (a<b)
			return a;
		else return b;
	}
	public static Punto [] introdPuntos(){
		Scanner leer = new Scanner(System.in);
		imprimir("¿Cuantos puntos hay?");
		int n =leer.nextInt();
		Punto [] puntos = new Punto[n];
		imprimir("Manual o Auto?");
		
		if (leer.next().charAt(1)!= 'a'){
			//Automatico|Aleatorio
			for(int i=0; i<puntos.length; i++){
				puntos[i] = new Punto((int)(Math.random()*100+1), 
						(int)(Math.random()*100+1));
			}
		}
		else{
			//manual
			for(int i=0; i<puntos.length; i++){
				imprimir("Coordenadas punto: " +i+ "\nX: ");
				double X = leer.nextDouble();
				imprimir("Y= ");
				double Y = leer.nextDouble();
				puntos[i]=new Punto(X,Y);
			}		
		}
		return puntos;
	}
	
	public static Punto [] parCercano(Punto A, Punto B){
		Punto [] parCercano = new Punto[2];
		parCercano [0] = A;
		parCercano [1] = B;
		return parCercano;
	}
	public static Punto[] divideYVenceras(Punto [] puntos, int n){
		double d;
		Punto [] resultado = new Punto[2];

		if (n<2){
			/*Si hay menos de dos puntos, descartamos esta division del plano
			haciendo que devuelva los puntos mas lejanos entre si posibles*/
			
			Punto a = new Punto(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
			Punto b = new Punto(Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY);
			return parCercano(a,b);
		}
		
		if (n==2){
			/*Que la division del plano tenga dos puntos significara 
			que hemos alcanzado el fin de la recursion*/

			resultado = parCercano(puntos[0], puntos[1]);
			return resultado;
		}
		/*Dividimos el plano, x1 representa al subplano izquierdo, 
		  x2 al derecho cada uno tiene la mitad de los puntos*/
		
		Punto [] x1 = new Punto[n/2];
		Punto [] x2 = new Punto[n/2 + n%2];
		
		for(int i=0; i<n/2; i++){
			x1[i] = puntos[i];
		}
		
		for(int i=0; i<(n/2 + n%2); i++){
			x2[i] = puntos[n/2+i];
		}
				
		int mid = n/2;
		Punto medio = puntos[mid];
		
		/* Ahora es cuando utilizamos la recursividad, 
		   dividimos entre izquierda y derecha. Despues pedimos al algoritmo
		   que identifique donde se encuentra el par de puntos mas cercano 
		 */
		
		Punto[] puntosIzq = divideYVenceras(x1, mid);
		Punto[] puntosDer = divideYVenceras(x2, n-mid);

		double dI = puntosIzq[0].distA(puntosIzq[1]);
		double dD = puntosDer[0].distA(puntosDer[1]);

		if (dI<dD){
			resultado = parCercano(puntosIzq[0],puntosIzq[1]);
			d=dI;
		}
		else{
			resultado = parCercano(puntosDer[0],puntosDer[1]);
			d=dD;
		}
		
		/* Crearemos una matriz auxiliar(vertAux) con los puntos 
		  "verticalmente problematicos", aquellos que se encuentran 
		  a una distancia menor que d a la linea de separacion entre 
		  los planos.
		  n sera un valor seguro para dimensionar 
		  la matriz y con la variable j que declararemos a continuacion, 
		  contaremos cuantos puntos contiene la matriz.
		 */
		
		Punto[] vertAux = new Punto[n];
		int j = 0;
		for (int i=0; i<n; i++){
			if ( Math.abs(puntos[i].getX() - medio.getX()) < d){
			vertAux[j]=puntos[i];
			j++;
			}
		}
		
		/* Pasamos los puntos contenidos en la matriz vertAux a la matriz
		   verticales y ya que ahora conocemos su dimension exacta (j), 
		   podremos darle el tamano apropiado */
		
		Punto[] verticales = new Punto[j];
		for(int i=0; i<j; i++){
			verticales[i]=vertAux[i];
		}
		/* Hallaremos el par mas cercano de tales puntos conflictivos 
		  verticalmente, si resulta que la distancia entre los puntos de
		  tal par es mas pequena que la que teniamos con el par que 
		  obtuvimos antes, sustituiremos el valor de resultado por tales
		  puntos, si no, lo ignoraremos.
		 */
		
		Punto [] vertCercanos = resolVerticales(verticales, d);
		if (vertCercanos[0].distA(vertCercanos[1]) < d){
			resultado = vertCercanos;
		}
		return resultado;

	}
	public static Punto[] resolVerticales(Punto[] verticales, double distancia){
		/*Provisionalmente el resultado es el par de puntos mas lejano posible
		Lo sustituiremos despues*/
		Punto a = new Punto(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
		Punto b = new Punto(Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY);
		Punto [] resultado = parCercano(a,b);
		//Ordenamos segun coordenada Y
		verticales = Quicksort.quicksortY(verticales,0,verticales.length-1);
		/* La distancia minima es de momento "d", la que obtuvimos 
		   con los horizontales*/
		double min = distancia;
		int size = verticales.length;
		
		if(verticales.length>1){
			for(int i = 0; i<size; i++){
				for(int j= i+1; j<size  && (Math.abs(verticales[j].getY() 
						- verticales[i].getY()) < min); j++){
					double distAux=verticales[i].distA(verticales[j]);
					if(min > distAux){
						min = distAux;
						resultado = parCercano(verticales[i], verticales[j]);
					}
				}
			}
		}
		return resultado;
	}
	public static Punto [] fuerzaBruta (Punto [] puntos){
		double distancia = Double.POSITIVE_INFINITY;
		Punto [] resultado = new Punto[2];
		if (puntos.length>1){
			for(int i=0; i<puntos.length; i++){
				for(int j=0; j<puntos.length && i!=j; j++){
					double distAux=puntos[i].distA(puntos[j]);
					if (distancia>distAux){
						resultado=parCercano(puntos[i], puntos[j]);
						distancia=distAux;
				
					}
				}
			}
		}
		else {
			Punto a = new Punto(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
			Punto b = new Punto(Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY);
			resultado = parCercano(a,b);
		}
		return resultado;
	}
}