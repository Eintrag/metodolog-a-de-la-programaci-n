public class Quicksort{
	 
public static void intercambia(Punto[] v, int pos1, int pos2){
	Punto aux=v[pos1];
	v[pos1]=v[pos2];
	v[pos2]=aux;
	}
public static Punto[] quicksortX(Punto[] v, int li, int ls){
	if (li<ls){
		int pos=divideX(v,li,ls);
		quicksortX(v, li, pos-1);
		quicksortX(v, pos+1, ls);
		}
	return v;
	}
public static int divideX(Punto[] v, int li, int ls){
	Punto pivote=v[li];
	int izq=li+1; int der=ls;
	while(izq<der && v[izq].getX()<=pivote.getX() ) izq++;
	while (v[der].getX()>pivote.getX()) der--;
	while (izq<der){
		intercambia(v,izq,der);
		do{izq++;}while(v[izq].getX()<=pivote.getX());
		do{der--;}while(v[der].getX()>pivote.getX());
		}
	intercambia(v,li,der);
return der;
	}
public static Punto[] quicksortY(Punto[] v, int li, int ls){
	if (li<ls){
		int pos=divideY(v,li,ls);
		quicksortY(v, li, pos-1);
		quicksortY(v, pos+1, ls);
		}
	return v;
	}
public static int divideY(Punto[] v, int li, int ls){
	Punto pivote=v[li];
	int izq=li+1; int der=ls;
	while(izq<der && v[izq].getY()<=pivote.getY() ) izq++;
	while (v[der].getY()>pivote.getY()) der--;
	while (izq<der){
		intercambia(v,izq,der);
		do{izq++;}while(v[izq].getY()<=pivote.getY());
		do{der--;}while(v[der].getY()>pivote.getY());
		}
	intercambia(v,li,der);
return der;
	}
}
