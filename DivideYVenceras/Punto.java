public class Punto {
	double x;
	double y;
	public Punto(double x, double y){
		this.x=x;
		this.y=y;
	}
	public double getX(){
		return x;
	}
	public double getY(){
		return y;
	}
	public String toString(){
		return "(X= "+x+ " Y= " +y+ ")";
	}
	public boolean equals(Punto p){
		return (x==p.getX() && y == p.getY());
	}
	public double distA(Punto p){
		double a = (x - p.getX());
		double b = (y - p.getY());
		return Math.sqrt(a*a + b*b);
	}
	//distancia
}
