
public class Gasolinera {
	int kmHastaProxima;
	String nombre;
	
	public Gasolinera(String name, int dHastaProxima){
		nombre=name;
		kmHastaProxima = dHastaProxima;
		
	}
	public Gasolinera(String name){
		//De la ultima parada no conocemos la distancia hasta la siguiente
		nombre=name;
	}
	public int getKmHastaProxima(){
		return kmHastaProxima;
	}
	public String getNombre(){
		return nombre;
	}
}
