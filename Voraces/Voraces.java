import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue; 

public class Voraces {
	public static void main(String [] args){
		int autonomia = 25;
		Queue <Gasolinera> gasolineras = introdGasolineras();
		
		double tInicio=System.currentTimeMillis();  
		List <Gasolinera> solucion = voraz(gasolineras, autonomia);
		double totalTiempo = System.currentTimeMillis() - tInicio; 
		System.out.println("El algoritmo tarda :" + totalTiempo + " miliseg"); 

		imprimir(solucion);
	}
	public static void imprimir(List <Gasolinera> solucion){
		String itinerario="";
		for(int i=0; i<solucion.size(); i++){
			itinerario += solucion.get(i).getNombre() +" ";
		}
		System.out.println(solucion.size() +" paradas. Itinerario = " + itinerario);
	}
	
	public static List <Gasolinera> voraz(Queue <Gasolinera> gasolineras, int autonomia){
		List <Gasolinera> solucion = new ArrayList<Gasolinera>();
		int deposito = autonomia;
		while(!gasolineras.isEmpty()){
			Gasolinera actual = gasolineras.remove();
			int recorrido = actual.getKmHastaProxima();
			if(gasolineras.size()==0){
				//La ultima parada debe ser incluida obligatoriamente en la solucion
				solucion.add(actual);
			}
			else if(recorrido <= deposito){
				//descontamos los km recorridos del deposito y no necesitamos parar
				deposito -= recorrido;
			}
			else{
				//Añadimos la gasolinera a la lista de paradas
				solucion.add(actual);
				//reposta
				deposito = autonomia;
				//sigue su camino
				deposito -= recorrido;
			}
		}
		
		return solucion;
	}
	
	public static Queue<Gasolinera> introdGasolineras(){
		Queue <Gasolinera> gasolineras = new LinkedBlockingQueue<Gasolinera>();
		String [] nombres = new String[9];
		int [] distancias = new int[8];
		nombres[0] = "Uno"; distancias[0] = 10;
		nombres[1] = "Dos"; distancias[1] = 15;
		nombres[2] = "Tres"; distancias[2] = 8;
		nombres[3] = "Cuatro"; distancias [3] = 20;
		nombres[4] = "Cinco"; distancias[4] = 12;
		nombres[5] = "Seis"; distancias[5] = 5;
		nombres[6] = "Siete"; distancias[6] = 6;
		nombres[7] = "Ocho"; distancias[7] = 21;
		nombres[8] = "Nueve";
		for(int i=0; i<distancias.length; i++){
			gasolineras.offer(new Gasolinera(nombres[i], distancias[i]));
		}
		gasolineras.offer(new Gasolinera(nombres[distancias.length]));
		
		
//		for(int i=0; i<1000; i++){
//			int dist = (int)(Math.random()*15);
//			gasolineras.offer(new Gasolinera("Gas",dist));
//		}
//		gasolineras.offer(new Gasolinera("Dos", (int)(Math.random()*15)));
		
//		
//		gasolineras.offer(new Gasolinera("Uno",10));
//		gasolineras.offer(new Gasolinera("Dos",15));
//		gasolineras.offer(new Gasolinera("Tres",8));
//		gasolineras.offer(new Gasolinera("Cuatro",20));
//		gasolineras.offer(new Gasolinera("Cinco",12));
//		gasolineras.offer(new Gasolinera("Seis",5));
//		gasolineras.offer(new Gasolinera("Siete",6));
//		gasolineras.offer(new Gasolinera("Ocho",21));
//		gasolineras.offer(new Gasolinera("Nueve"));
		return gasolineras;
	}
}
